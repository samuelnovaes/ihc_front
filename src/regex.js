export default {
	required: /^.+$/i,
	nome: /^([a-z]+ ?)+$/i,
	link: /^https?:\/\/.*\.[a-z\d]+(\/.*)?$/i,
	email: /^([a-z]+\.?)+@(estudante\.)?ifb\.edu\.br$/i,
	emailEstudante: /^([a-z]+\.?)+@estudante\.ifb\.edu\.br$/i,
	data: /^\d{2}\/\d{2}\/\d{4}$/,
	senha: /^.{6,}$/,
	telefone: /^(\(\d{2}\)|\d{2})? ?9?\d{4}-?\d{4}$/,
	integer: /^\d+$/,
	genericEmail: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i
}
