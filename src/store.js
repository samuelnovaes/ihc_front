export default {
	state: {
		login: false
	},
	mutations: {
		login(state, obj){
			state.login = obj
		},
		logoff(state){
			state.login = false
		}
	}
}
