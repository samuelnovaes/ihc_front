import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueRouter from 'vue-router'
import routes from './routes.js'
import Vuex from 'vuex'
import store from './store.js'

//Components
import noticia from './components/noticia.vue'
import titulo from './components/titulo.vue'
import inputFile from './components/input-file.vue'
import progresso from './components/progresso.vue'

Vue.component('noticia', noticia)
Vue.component('titulo', titulo)
Vue.component('input-file', inputFile)
Vue.component('progresso', progresso)

//Vue config
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(Vuetify, {
	theme: {
		primary: '#37963F',
		secondary: '#424242',
		accent: '#82B1FF',
		error: '#FF5252',
		info: '#2196F3',
		success: '#4CAF50',
		warning: '#FFC107'
	}
})
Vue.use({
	install(V, O) {
		V.prototype.$axios = axios
	}
})
Vue.use({
	install(V, O) {
		V.mixin({
			methods: {
				$logoff(){
					this.$store.commit('logoff')
					delete localStorage.login
					this.$router.push('/')
					alert('Sua sessão foi encerrada!')
				}
			}
		})
	}
})

new Vue({
	el: '#app',
	router: new VueRouter({routes}),
	store: new Vuex.Store(store),
	render: h => h(App)
})
