# INSTRUÇÕES

Website hospedado em http://200.130.152.86:8001/

- Rodar

```
git clone https://gitlab.com/samuelnovaes/ihc_front.git
cd ihc_front
npm install
npm run dev
```

- Mandar pro GitLab

```
git add .
git commit -m "sua descrição aqui"
git push origin master
```

Se der algum erro, dê um `git pull` antes de enviar, pode ser que funcione ;)

- Configurar projeto Cordova

Basta dar um `npm run build` no projeto e copiar `dist`, `public` e `index.html` para a pasta www do projeto Cordova.