FROM node:carbon
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . /usr/src/app/
RUN npm run build
EXPOSE 5000
CMD [ "npm", "start" ]
